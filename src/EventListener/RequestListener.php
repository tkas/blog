<?php 
namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use App\Service\IpLog;
use Symfony\Component\HttpKernel\EventListener\ResponseListener;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class RequestListener{
    
    private $log;
    public function __construct(IpLog $log){
        $this->log = $log;
    }
    
    public function onKernelResponse(FilterResponseEvent $fre){
 
        if($fre->getRequestType() === 1 && $fre->getRequest()->attributes->get('_route')!= '_wdt' ){
            $route = $fre->getRequest()->getRequestUri();
            //$ipLog = new IpLog();
            $this->log->saveVisitorIp($route);
        }
        
        
        
        
        
    }
    
}

?>