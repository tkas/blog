<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Utils\Blog;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content',TextareaType::class)
            ->add('locale',ChoiceType::class,array('choices'=> Blog::locale()))            
            ->add('status', ChoiceType::class, array(
                'choices'  => Blog::publishOptions() ,))           
            ->add('comment_status', ChoiceType::class, array(
                'choices'  => Blog::commentOptions(),))        
            ->add('category')
            ->add('tags')            
            ->add('home_image',FileType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
