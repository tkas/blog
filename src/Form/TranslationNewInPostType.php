<?php

namespace App\Form;

use App\Entity\Translation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Utils\Blog;


class TranslationNewInPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('original')
            ->add('type',ChoiceType::class,array('choices'=> Blog::postTranslationFields()))
           
            ->add('value')
            ->add('locale',ChoiceType::class,array('choices'=> Blog::locale()))
            ->add('idRelation',HiddenType::class)
            ->add('class')
        ;
      
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Translation::class,
        ]);
    }
}
