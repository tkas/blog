<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('email')
        ->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'first_options'  => array('label' => 'Mot de passe', 'attr'=> array('class'=>'form-control','placeholder'=>'Mot de passe')),
            'second_options' => array('label' => 'Confirmer le mot de passe','attr'=> array('class'=>'form-control','placeholder'=>'Confirmer le mot de passe')),
        ))
        ->add('firstName')
        ->add('LastName')
        ->add('birthDay',DateType::class,array(
            'html5'=>true,
            'widget' => 'single_text',
            'attr'=>array('type'=>'date')
        ))
        ->add('username')        
       
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }
}
