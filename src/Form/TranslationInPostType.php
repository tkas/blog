<?php

namespace App\Form;

use App\Entity\Translation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class TranslationInPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('original')
            ->add('type',HiddenType::class);
            
            if($options['data']->getType() == 'content'){                
            $builder->add('value',TextareaType::class);              
            }
            elseif($options['data']->getType() == 'title'){                
                $builder->add('value',TextType::class);                
            }else{
                $builder->add('value');                
            }
            
           $builder 
            ->add('locale',HiddenType::class)
            ->add('idRelation',HiddenType::class)
            ->add('class',HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Translation::class,
        ]);
    }
}
