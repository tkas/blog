<?php 
namespace App\Utils;

class Blog{
    static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('#[^\\pL\d]+#u', '-', $text);
        
        // trim
        $text = trim($text, '-');
        
        // transliterate
        if (function_exists('iconv'))
        {
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        }
        
        // lowercase
        $text = strtolower($text);
        
        // remove unwanted characters
        $text = preg_replace('#[^-\w]+#', '', $text);
        
        if (empty($text))
        {
            return 'n-a';
        }
        
        return $text;
    }
    
    static public function publish($text){
        $data = array(
            'publish'=>'Publié',
            'attente'=> 'En attente',
            'notpublish' => 'Non publié',
            'yes'=>'oui',
            'no'=>'non'
            
        );
        
        if(array_key_exists($text, $data)){
            return $data[$text];
        }
        return $text;       
        
        
    }

    
    static public function publishOptions(){       
            $options = array(
                'Publié' =>  'publish',
                'En attente' => 'attente',
                'Non publié' => 'notpublish',
            );
            return $options;       
    }
    
    
    static public function commentOptions(){
        $options = array(
            'Activé' =>  'actived',
            'Désactivé' => 'deactivate'
           
        );
        return $options;    
    }
    

    
    static public function locale()
    {
        return array('Français'=>'fr','English'=>'en', 'Español'=>'es');
    }
    
    static public function postTranslationFields()
    {
        return array('title'=>'title','content'=>'content');
    }
    
    
    
    
    static public function translate($objects,$translations,$fields){        
        
        foreach($translations as $value){
            if(array_key_exists($value['type'],$fields)){
                $translation[$value['idRelation']][$value['type']] = $value['value'];
            }else{
                $translation[$value['idRelation']][][$value['type']] = $value['value'];
            }
        }        
      
        if(is_array($objects)){
            foreach($objects as $k=>$object){
                if(array_key_exists($object->getId(), $translation)){
                    foreach($fields as $field){
                        $setter = 'set'.ucfirst($field);
                        if(method_exists(get_class($object),$setter))
                        {
                            $object->$setter($translation[$object->getId()][$field]);
                        }
                    }
                }
                
            }
        }else{
            if(array_key_exists($objects->getId(), $translation)){
                foreach($fields as $field){
                    $setter = 'set'.ucfirst($field);
                    if(method_exists(get_class($objects),$setter))
                    {
                        $objects->$setter($translation[$objects->getId()][$field]);
                    }
                }
            }
        }
        return $objects;
    }
    
    
    
    
}


    ?>