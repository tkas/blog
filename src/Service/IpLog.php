<?php
namespace App\Service;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;


class IpLog
{
  private function getRealUserIp(){
        switch(true){
            case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
            case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
            case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
            default : return $_SERVER['REMOTE_ADDR'];
        }
    }
    
       
    public function saveVisitorIp($route){
        
     
        
        
      $fs = new Filesystem();
      $file = '../var/log/ipLog.txt';
        if(!$fs->exists($file)){
            $fs->touch($file);        
        }
                
        $serializer = new Serializer(array(new DateTimeNormalizer('d/m/Y H:i:s')));
        $date = $serializer->normalize(new \DateTime());       
        
        $content =  $date.' - '.$this->getRealUserIp().' - '.$route."\n";
        $fs->appendToFile($file, $content);
    }
    
    
}

