<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use App\Entity\Tag;
use App\Entity\User;
use App\Entity\Comment;
use App\Entity\Post;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $cat = array('informatique','automobile','programmation','sport','musique','graphisme');
        
      
            $category = new Category();           
            $category->setName(ucfirst('informatique'));
            $category->setSlug('informatique');
            $this->addReference('category-informatique', $category);            
            $informatique = $this->getReference('category-informatique');            
            $manager->persist($category);
            
            $category1 = new Category();
            $category1->setName(ucfirst('graphisme'));
            $category1->setSlug('graphisme');
            $this->addReference('category-graphisme', $category1);
            $graphisme = $this->getReference('category-graphisme');
            $manager->persist($category1);
            $manager->flush();
   
            $category2 = new Category();
            $category2->setName(ucfirst('photo'));
            $category2->setSlug('photo');
            $this->addReference('category-photo', $category2);
            $photo = $this->getReference('category-photo');
            $manager->persist($category2);
            $manager->flush();
            
            $category3 = new Category();
            $category3->setName(ucfirst('jeux'));
            $category3->setSlug('jeux');
            $this->addReference('category-jeux', $category3);
            $jeux = $this->getReference('category-jeux');
            $manager->persist($category3);
            $manager->flush();
            
            
            
            $category4 = new Category();
            $category4->setName(ucfirst('cinema'));
            $category4->setSlug('cinema');
            $this->addReference('category-cinema', $category4);
            $cinema = $this->getReference('category-cinema');
            $manager->persist($category4);
            $manager->flush();
            
            
            
            
        
        $tags = array('informatique','automobile','programmation','sport','musique','graphisme');
        
        foreach($tags as $v){
            $tag = new Tag();
            $tag->setName(ucfirst($v));          
            $manager->persist($tag);
            $manager->flush();
        }
        
        
        
        $user = new User();
        $user->setPseudonyme('tek');
        $user->setEmail('nocif30@gmail.com');
        $user->setPassword('test');
        $user->setActivation('actived');
        $user->setFirstName('Masson');
        $user->setLastName('Yann');
        $user->setRegistredAt(new \DateTime('2018/01/01 10:28:07'));
        $user->setBirthDay(new \DateTime('1985-12-23 18:15:01'));
        $user->setRole('admin');
        $manager->persist($user);
    
        
        $user2 = new User();
        $user2->setPseudonyme('admin');
        $user2->setEmail('admin@gmail.com');
        $user2->setPassword('admin');
        $user2->setActivation('actived');
        $user2->setFirstName('Admin');
        $user2->setLastName('Admin');
        $user2->setRegistredAt(new \DateTime('2018/05/01 10:28:07'));
        $user2->setBirthDay(new \DateTime('2018-01-03'));
        $user2->setRole('admin');
        $manager->persist($user2);
      
        
        
        $user3 = new User();
        $user3->setPseudonyme('member');
        $user3->setEmail('member@gmail.com');
        $user3->setPassword('member');
        $user3->setActivation('actived');
        $user3->setFirstName('member');
        $user3->setLastName('member');
        $user3->setRegistredAt(new \DateTime('2018/05/01 10:28:07'));
        $user3->setBirthDay(new \DateTime('2018-01-03'));
        $user3->setRole('member');
        $manager->persist($user3);
        $manager->flush();
        
        $this->addReference('user-admin', $user);
        $this->addReference('user-member', $user3);
        
        $admin = $this->getReference('user-admin');
        $member = $this->getReference('user-member');
            
        
        
        $post1 = new Post();
        $post1->setUser($admin);
        $post1->setTitle('Formation photo : en immersion à la Nikon School');
        $post1->setContent('
Huit lecteurs de Presse-citron ont testé la formation à la photo à la Nikon School. On partage leur expérience avec vous en vidéo !
La Nikon School est devenue une véritable institution de la formation à la photo à Paris. Des milliers d’apprentis photographes y ont appris les bases et les techniques avancées. La Nikon School est installée au sein du Nikon Plaza, ce nouveau lieu dont Éric vous avait proposé la visite guidée en live.
On a proposé à nos lecteurs de tester la formation à la Nikon School. Huit d’entre vous, particulièrement motivés pour progresser en photo, ont ainsi été sélectionnés pour participer à une formation spéciale.
Voici un petit aperçu en vidéo de l’expérience qu’ils ont pu vivre à la Nikon School. On retiendra qu’avec ne serait-ce qu’une seule journée de formation, on fait de sacré progrès en photo !
');
        $post1->setStatus('publish');
        $post1->setSlug('Formation-photo-:-en-immersion-à-la-Nikon-School');
        $post1->setCommentStatus('actived');
        $post1->setCreatedAt(new \DateTime('2018/06/12 15:32:07'));
        $post1->addCategory($informatique);
      
        $post1->setHomeImage('Nikon-School.png');
        $manager->persist($post1);
        $manager->flush();
        
        $this->addReference('post-first', $post1);        
        $postFirst = $this->getReference('post-first');
        
        
        
        $post2 = new Post();
        $post2->setUser($member);
        $post2->setTitle('E3 2018 : Super Smash Bros Ultimate, ou le Menu Maxi Best of sur Nintendo Switch');
        $post2->setContent('
Nintendo a dévoilé en détail son tout nouveau Super Smash Bros attendu pour le 7 décembre sur Nintendo Switch. Un épisode qui se veut plus « Ultimate » que jamais.
Super Smash Bros se fait Ultimate sur Nintendo Switch

Le récent Nintendo Direct diffusé hier soir par le géant nippon a permis notamment d’officialiser l’arrivée de Fortnite sur Nintendo Switch, mais aussi d’en apprendre davantage sur quelques-uns des titres à venir. Une conférence marquée toutefois par l’absence de titres tels que Yoshi, Metroid Prime 4 ou même Pokémon (sans oublier un F-Zero tant demandé par certains fans), mais qui a en contrepartie accordé une place très (trop ?) importante à un autre hit à venir : Super Smash Bros Ultimate.

En effet, en plus d’officialiser l’appellation définitive de cet opus, le Nintendo Direct a permis, pendant plus de 20 minutes, de découvrir en détail ce nouvel opus, attendu pour le 7 décembre prochain. Super Smash Bros Ultimate combine ainsi de nombreux stages et de nouveaux objets à un gameplay permettant à tous les joueurs de s’amuser, quel que soit leur niveau. Toutes les figurines amiibo de la série Super Smash Bros. sont compatibles avec le jeu, et les figurines amiibo de chaque combattant en dehors de la série sont également compatibles.


Mieux encore, Nintendo a confirmé que ce Super Smash Bros Ultimate inclut tous les personnages figurant dans les épisodes de cette franchise qui a vu le jour il y a près de vingt ans sir Nintendo 64, ce qui fait de cet opus l’un des titres cross-over les plus vastes de toute l’histoire du jeu vidéo. Les fans pourront profiter du retour de personnages appréciés tels que les Ice Climbers ou les dresseurs de Pokémon, mais ils découvriront aussi de nouveaux combattants comme Ridley de la série Metroid et les Inklings de la franchise Splatoon. Le titre propose en outre diverses options de contrôle, dont les manettes Nintendo GameCube (originales ou récentes), les Joy-Con ou encore la manette Nintendo Switch Pro.

Le « plus grand Super Smash Bros jamais créé » a donc eu droit à une présentation très complète de la part de Nintendo, presque trop selon certains fans, tant la vidéo spoilait le contenu du jeu. Toujours est-il que Super Smash Bros Ultimate sera clairement l’un des jeux évènements de cette fin d’année 2018, avec 66 combattants fermement attendus pour le 7 décembre, exclusivement sur Nintendo Switch.


');
      

        
        $post2->setStatus('publish');
        $post2->setSlug('Super-Smash-Bros-Ultimate');
        $post2->setCommentStatus('actived');
        $post2->setCreatedAt(new \DateTime('2018/06/13 18:22:07'));
        $post2->addCategory($graphisme);
        $post2->setHomeImage('Megaman-Smash-Bros-Ultimate.jpg');
        $manager->persist($post2);
        $manager->flush();
        
        $this->addReference('post-second', $post2);
        $postSecond = $this->getReference('post-second');
        
        
        
        
        
        
        
        $post3 = new Post();
        $post3->setUser($member);
        $post3->setTitle('The Elder Scrolls VI est en pré-production');
        $post3->setContent('
Après le retour de Diablo, c’est désormais le nouvel opus de la saga The Elder Scrolls qui a été annoncé à l’E3 lors de la conférence du studio Bethesda. Toutefois, prendre la suite de Skyrim ressemble tout de même à un sacré défi.

The Elder Scrolls VI : une bonne nouvelle…

Bethesda a montré que comme éditeur, il état passé au niveau supérieur lors de l’E3, avec un niveau de présentation méconnu tout d’abord, mais aussi avec des jeux multijoueurs qui devraient toucher un public toujours plus large. De plus, le studio a frappé très fort en annonçant lors d’une même conférence Fallout 76, Starfield, Blade et The Elder Scrolls VI. Enfin, l’éditeur a aussi dérogé à ses habitudes en annonçant des jeux qui ne sont pas encore sur le point de sortir.

Malheureusement, c’est bel et bien le cas de The Elder Scrolls VI. Il n’y a pas encore de date de sortie connue, loin de là. On en serait même encore au stade de la « pré-production ». Et ce, alors que Skyrim est désormais sorti depuis 2011 ! Pour le moment, vous n’aurez le droit qu’à 20 secondes d’une vidéo de présentation pour faire quand même un peu de teasing. « Starfield est jouable. Elder Scrolls 6 pas vraiment pour le moment » résume Bethesda.
… à condition d’être patient !

Concrètement, le jeu en est encore à la phase où l’on discute du gameplay, de l’histoire, des éléments graphiques… Beaucoup de travail reste encore entre les mains des développeurs. En attendant voici une petite vidéo pour vous donner un aperçu de ce futur jeu.

« On prépare la génération à venir » a résumé Todd Howard, game designer en charge du projet. L’objectif semble au mieux être 2020 ou 2021. Autant dire que le jeu pourrait être destiné à la prochaine génération de consoles ou même au streaming si les prédictions d’Yves Guillemot, le PDG du studio Ubisoft se révèlent exactes. Dans tous les cas, vous pouvez retourner sur Skyrim, le jeu n’est pas encore bon pour la retraite.
 
            
');
        
        $post3->setStatus('Publié');
        $post3->setSlug('The-Elder-Scrolls-VI');
        $post3->setCommentStatus('actived');
        $post3->setCreatedAt(new \DateTime('2018/04/25 06:01:49'));
        $post3->addCategory($jeux);
        $post3->setHomeImage('The-Elder-Scrolls-VI.jpg');
        $manager->persist($post3);
        $manager->flush();
        
        $this->addReference('post-third', $post3);
        $postThird = $this->getReference('post-third');
        
        
        

        
        
        
        $post4 = new Post();
        $post4->setUser($admin);
        $post4->setTitle('Avengers: Infinity War');
        $post4->setContent('
Les Avengers et leurs alliés devront être prêts à tout sacrifier pour neutraliser le redoutable Thanos avant que son attaque éclair ne conduise à la destruction complète de l’univers.
          
');
        
        $post4->setStatus('publish');
        $post4->setSlug('Avengers-Infinity-War');
        $post4->setCommentStatus('actived');
        $post4->setCreatedAt(new \DateTime('2017/12/23 22:34:09'));
        $post4->addCategory($cinema);
        $post4->setHomeImage('f1.jpg');
        $manager->persist($post4);
        $manager->flush();
        
        $this->addReference('post-four', $post4);
        $postFour = $this->getReference('post-four');
        
        
        
        
        
        
        $post5 = new Post();
        $post5->setUser($member);
        $post5->setTitle('Cinquante nuances plus claires');
        $post5->setContent('
Plus heureux que jamais, Anastasia et Christian forment désormais un couple solide. Cependant, leur bonheur est troublé par Jack Hyde, l\'ancien patron d\'Anastasia et par une nouvelle qui pourrait détruire leur relation.           
');
        
        $post5->setStatus('publish');
        $post5->setSlug('Cinquante-nuances-plus-claires');
        $post5->setCommentStatus('actived');
        $post5->setCreatedAt(new \DateTime('2018/03/14 16:01:54'));
        $post5->addCategory($cinema);
        $post5->setHomeImage('f2.jpg');
        $manager->persist($post5);
        $manager->flush();
        
        $this->addReference('post-five', $post5);
        $postFive = $this->getReference('post-five');
        
        
        
    
        
        
        
        
        
        

        
        
        
        $comment = new Comment();
        $comment->setPost($postFirst);
        $comment->setUser($admin);
        $comment->setContent('Premier commentaire en tant qu\'admin');
        $comment->setApprouved('Approuved');
        $comment->setCreatedAt(new \DateTime('2018/06/13 10:28:07'));
        
        $manager->persist($comment);
        $manager->flush();
        
        $comment1 = new Comment();
        $comment1->setPost($postFirst);
        $comment1->setUser($member);
        $comment1->setContent('Commentaire d\'un membre en réponse à un commentaire précedant');
        $comment1->setApprouved('Approuved');
        $comment1->setCreatedAt(new \DateTime('2018/06/13 20:28:07'));
        
        $manager->persist($comment1);
        $manager->flush();
        
        $comment2 = new Comment();
        $comment2->setPost($postSecond);
        $comment2->setUser($admin);
        $comment2->setContent('Commentaire d\'un admin sur le post d\'un membre.');
        $comment2->setApprouved('Approuved');
        $comment2->setCreatedAt(new \DateTime('2018/06/13 20:28:07'));
        
        $manager->persist($comment2);
        $manager->flush();
        
        
    }
}
