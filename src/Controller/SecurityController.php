<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Form\User1Type;

class SecurityController extends Controller
{
    
    /**
     * @Route("/{_locale}/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {        
        $error = $authenticationUtils->getLastAuthenticationError();        
       
        $lastUsername = $authenticationUtils->getLastUsername();
        
        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));        
    }
    
    /**
     * @Route("/{_locale}/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder,\Swift_Mailer $mailer)
    {
        $user= new User();
        $form = $this->createForm(User1Type::class, $user);        
        $form->handleRequest($request);    
        
        if($form->isSubmitted() && $form->isValid()){
            $user->setRoles('ROLE_USER');
            $user->setisActive(0);
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);      
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success','Votre inscription a bien été prise en compte. Veuillez vous connecter.');
            
            $message = (new \Swift_Message('Bienvenue'))
            ->setFrom('admin@blog.com')
            ->setTo($user->getEmail())
            ->setBody($this->renderView('emails/registration.html.twig', array('user' => $user)),'text/html');            
            $mailer->send($message);            
            
            
            
            return $this->render('security/login.html.twig',array(
                'last_username'=>$user->getUsername(),
                'error'=>null
            ));
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le formulaire d\'inscription n\'est pas valide. Veuillez vérifier les champs.');
        }  
        
        return $this->render('security/register.html.twig', array(
          'form'=>$form->createView()
        ));
    }
    
}