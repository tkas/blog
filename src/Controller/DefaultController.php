<?php
namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use App\Entity\Translation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Entity\User;
use App\Repository\TranslationRepository;
use App\Utils\Blog;
use App\Repository\PartnerRepository;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="base")
     */
    public function base(Request $request)
    {    
       return $this->redirectToRoute("home");
    }
    
    
    /**
     * @Route("/{_locale}/partners", name="home")
     */
    public function partners(PartnerRepository $partnersRepository)
    {    
        return $this->render('partner/home.html.twig', array('partners'=> $partnersRepository->findRandom()));
    }
    
    
    /**
     * @Route("/{_locale}/{page}", name="blog", methods="GET", requirements={"page" = "\d+"}, defaults={"page" = 1})
     */
    public function index(PostRepository $postRepository,$page, Request $request,TranslationRepository $tr): Response
    {       
      
        $totalPosts = $postRepository->countAllPostPublish();        
        $postOnPage = $this->container->getParameter('max_post_on_home');
        $last_page = ceil($totalPosts / $postOnPage);
        $previous_page = $page > 1 ? $page - 1 : 1;
        $next_page = $page < $last_page ? $page + 1 : $last_page;       
      
        $post = $postRepository->findHomePost($postOnPage,($page-1)*$postOnPage);
        $translations = $tr->findTranslation(Post::class,null,$request->getLocale(),null);
        $post = Blog::translate($post, $translations, blog::postTranslationFields());
        
       return $this->render('default/home.html.twig', array(
            'posts' => $post,
            'last_page' => $last_page,
            'previous_page' => $previous_page,
            'current_page' => $page,
            'next_page' => $next_page,
            'totalPosts' => $totalPosts
        ));        
    }
    
    /**
     * @Route("/{_locale}/admin", name="admin_home", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function indexAdmin(): Response
    {
        
        /**
         * 
         * a nettoyer trop de requete inutile (faire du tri avec des fonctions php)
         * 
         */        
        
        $emCom = $this->getDoctrine()->getRepository(Comment::class);
        $countComNotApproved = $emCom->countNotApprovedComment();
        $countComTotal = $emCom->countComTotal();
        $countComParent = $emCom->countComParent();
        $countComResponse = $emCom->countComResponse();
        
        
        $emUser = $this->getDoctrine()->getRepository(User::class);      
        $countUserTotal = $emUser->countUserTotal();
        $countUserNotApproved = $emUser->countUserNotApproved();
        
        
        $emPost = $this->getDoctrine()->getRepository(Post::class);     
        $countPostPublish = $emPost->countAllPostPublish();        
        
        
        return $this->render('default/admin.home.html.twig',array(
            'countComNotApproved'=> $countComNotApproved,
            'countUserTotal'=>$countUserTotal,
            'countUserNotApproved' => $countUserNotApproved,
            'countComTotal' => $countComTotal,
            'countPostPublish' => $countPostPublish,
            'countComParent' => $countComParent,
            'countComResponse' => $countComResponse
            
        ));
        
    }
    
    /**
     * @Route("/{_locale}/mail", name="mail", methods="GET")
     */
    public function mail(\Swift_Mailer $mailer): Response
    { 
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('nocif30@gmail.com')
        ->setTo('masson.ys@free.fr')
        ->setBody($this->renderView('emails/registration2.html.twig', array('user' => 'test')),'text/html');
        
        $mailer->send($message);            
       
        return $this->render('emails/registration2.html.twig', array('user' => 'test'));        
    }
    
    /**
     * @Route("/{_locale}/search", name="search", methods="GET")
     */
    public function search(Request $request): Response
    {
       
       $search = $request->query->get('search');
       $result= [];
       
       
       $emTranslation = $this->getDoctrine()->getRepository(Translation::class);
       $translations = $emTranslation->findTranslation(Post::class,null,$request->getLocale(),null,$search);       
       
       if($translations){           
       foreach($translations as $value){
           $listPosts[]=$value['idRelation'];
       }
       $emPost = $this->getDoctrine()->getRepository(Post::class);
       $posts = $emPost->findForSearch($listPosts);
       $tra = $emTranslation->findTranslation(Post::class,$listPosts,$request->getLocale(),null);       
       $result['Post'] = blog::translate($posts, $tra, blog::postTranslationFields());             
       }else{
           $result['Post'] = [];           
       }  
       
       $emComment = $this->getDoctrine()->getRepository(Comment::class);
       $result['Comment'] = $emComment->findForSearch($search);        
       
       $emUser = $this->getDoctrine()->getRepository(User::class);
       $result['User'] = $emUser->findForSearch($search);  
       
        dump($result);
        return $this->render('default/search.html.twig',array(
            'result' => $result
        ));
    }
    
    


    
    
}

