<?php

namespace App\Controller;

use App\Entity\Comment;

use App\Form\ComType;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\EventDispatcher\GenericEvent;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/{_locale}/comment")
 */
class CommentController extends Controller
{
    /**
     * @Route("/admin/", name="admin_comment_index", methods="GET")
     */
    public function index(CommentRepository $commentRepository): Response
    {
        return $this->render('comment/admin/index.html.twig', ['comments' => $commentRepository->findAll()]);
    }

     /**
     * @Route("/postNew/{id}/{postSlug}", name="post_comment_new", methods="POST")
     * @ParamConverter("post", options={"mapping": {"id":"id"}})
     */
    public function newFromPost(Request $request,Post $post): Response
    {
        $comment = new Comment();       
        $form = $this->createForm(ComType::class, $comment);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setApproved('no');
            $comment->setPost($post);
            $comment->setUser($this->getUser());                 
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success','Le commentaire a bien été ajouté.');
            return $this->redirectToRoute('post_show',['id'=>$post->getId(), 'slug'=>$post->getSlug()]);
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le commentaire n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        return $this->render('comment/new.post.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
  
    }
    /**
     * @Route("/replyNew/{idComment}/{postId}/{postSlug}", name="post_reply_comment_new", methods="POST|GET")
     * @ParamConverter("comment", options={"mapping": {"idComment": "idComment"}})
     * @ParamConverter("post", options={"mapping": {"postId":"id"}})
     */
    public function newReplyFromPost(Request $request,Comment $origin, Post $post): Response
    {
        $reply = new Comment();       
        $form = $this->createForm(ComType::class, $reply);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $reply->setApproved('no');      
            $origin->addComment($reply);       
            $reply->setOrigin($origin);
            $reply->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($reply);
            $em->flush();
            $this->addFlash('success','Le commentaire a bien été ajouté.');
            return $this->redirectToRoute('post_show',['id'=>$post->getId(), 'slug'=>$post->getSlug()]);
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le commentaire n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        return $this->render('comment/new.reply.post.html.twig', [
            '$post' => $post,
            'form' => $form->createView(),
        ]);        
    }    
        
    /**
     * Utilisation en appel direct à partir du template twig
     * @param Post $post
     * @return Response
     */
    public function commentForm(Post $post): Response
    {
        $form = $this->createForm(ComType::class);        
        return $this->render('comment/new.post.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Utilisation en appel direct à partir du template twig
     * @param Post $post
     * @return Response
     */
    public function commentReplyForm(Post $post,Comment $comment): Response
    {
        $form = $this->createForm(ComType::class);
        return $this->render('comment/new.reply.post.html.twig', [
            'post' => $post,
            'comment'=>$comment,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/admin/{id}", name="admin_comment_show", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function show(Comment $comment): Response
    {
        return $this->render('comment/admin/show.html.twig', ['comment' => $comment]);
    }

    /**
     * @Route("/admin/{id}/edit", name="admin_comment_edit", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, Comment $comment): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Le commentaire a bien été modifié.');
            return $this->redirectToRoute('admin_comment_index');
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le commentaire n\'est pas valide. Veuillez vérifier les champs.');
        }        
        
        return $this->render('comment/admin/edit.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_delete", methods="DELETE")  
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Comment $comment): Response
    {        
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
            $this->addFlash('success','Le commentaire a bien été supprimé.');
        }
        return $this->redirectToRoute('home');
    }
}
