<?php

namespace App\Controller;

use App\Entity\Translation;
use App\Form\TranslationInPostType;
use App\Form\TranslationNewInPostType;
use App\Form\TranslationType;
use App\Repository\TranslationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;

/**
 * @Route("/{_locale}/translation")
 */
class TranslationController extends Controller
{
    /**
     * @Route("/", name="translation_index", methods="GET")
     */
    public function index(TranslationRepository $translationRepository): Response
    {
        return $this->render('translation/index.html.twig', ['translations' => $translationRepository->findAll()]);
    }
   

    /**
     * @Route("/new", name="translation_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $translation = new Translation();
        $form = $this->createForm(TranslationType::class, $translation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($translation);
            $em->flush();

            return $this->redirectToRoute('translation_index');
        }

        return $this->render('translation/new.html.twig', [
            'translation' => $translation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="translation_show", methods="GET")
     */
    public function show(Translation $translation): Response
    {
        return $this->render('translation/show.html.twig', ['translation' => $translation]);
    }

    /**
     * @Route("/{id}/edit", name="translation_edit", methods="GET|POST")
     */
    public function edit(Request $request, Translation $translation): Response
    {
        $form = $this->createForm(TranslationType::class, $translation);
        $form->handleRequest($request);
        
  
     

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('translation_edit', ['id' => $translation->getId()]);
        }

        return $this->render('translation/edit.html.twig', [
            'translation' => $translation,
            'form' => $form->createView(),
        ]);
    }
    
    
    /**
     * @Route("/{id}/editInPost", name="translation_edit_in_post", methods="GET|POST")
     */
    public function editInPost(Request $request, Translation $translation): Response
    {
        $form = $this->createForm(TranslationInPostType::class, $translation);
        $form->handleRequest($request);        
           
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            
            
            $pr= $this->getDoctrine()->getRepository(Post::class);
            $post = $pr->find($translation->getIdRelation());
            
            return $this->redirectToRoute('admin_post_edit', ['id'=>$post->getId(),'slug' => $post->getSlug() ]);
        }
        
        return $this->render('translation/_formPost.html.twig', [
            'translation' => $translation,
            'form' => $form->createView(),
        ]);
    }
    
    
    
       
    
    
    /**
     * @Route("/newInPost/{idRelation}", name="translation_new_in_post", methods="GET|POST")
     */
    public function newInPost(Request $request,$idRelation=null,TranslationRepository $tr): Response
    {
        $translation = new Translation(); 
        $translation->setIdRelation($idRelation);       
        $translation->setOriginal(0);
        $translation->setClass(Post::class);
        
      
       
        $form = $this->createForm(TranslationNewInPostType::class, $translation);
        $form->handleRequest($request);
        
   
        
        
        if ($form->isSubmitted() && $form->isValid()) {
            if($tr->findOneBy(array('idRelation'=>$translation->getIdRelation(),'type'=>$translation->getType(),'locale'=>$translation->getLocale(),'class'=>$translation->getClass()))){
               
                dump($tr->findOneBy(array('type'=>$translation->getType(),'locale'=>$translation->getLocale(),'class'=>$translation->getClass())));
                
                
                $this->addFlash('danger', 'Une traduction existe déjà pour cette locale et ce type');
                
            }else{
            $em = $this->getDoctrine()->getManager();
            $em->persist($translation);
            $em->flush();
            }
            $pr= $this->getDoctrine()->getRepository(Post::class);
            $post = $pr->find($translation->getIdRelation());            
            return $this->redirectToRoute('admin_post_edit', ['slug' => $post->getSlug() ]);
            
        }
        
        return $this->render('translation/_formNewInPost.html.twig', [
            'translation' => $translation,
            'form' => $form->createView(),
            'idRelation' => $idRelation
        ]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    /**
     * @Route("/{id}", name="translation_delete", methods="DELETE")
     */
    public function delete(Request $request, Translation $translation): Response
    {
        if ($this->isCsrfTokenValid('delete'.$translation->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($translation);
            $em->flush();
        }

        return $this->redirectToRoute('translation_index');
    }
}
