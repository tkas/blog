<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Form\TagType;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/{_locale}/tag")
 */
class TagController extends Controller
{
    /**
     * @Route("/", name="admin_tag_index", methods="GET")
     */
    public function index(TagRepository $tagRepository): Response
    {
        return $this->render('tag/admin/index.html.twig', ['tags' => $tagRepository->findAll()]);
    }
    
    

    /**
     * @Route("/admin/new", name="admin_tag_new", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request): Response
    {
        $tag = new Tag();        
        $form = $this->createForm(TagType::class, $tag);     
        $form->handleRequest($request);
       
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();
            $this->addFlash('success','Le tag a bien été ajouté.');
            return $this->redirectToRoute('admin_tag_index');
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le tag n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        return $this->render('tag/admin/new.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
       
        ]);
    }
    
    /**
     * @Route("/ajaxNew", name="ajax_tag_new", methods="GET|POST")
     */
    public function ajaxNew(Request $request): Response
    {
        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();          
            return $this->json($tag);
        }
        
        return $this->render('tag/admin/ajax.new.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
        ]);
    }   
        


    /**
     * @Route("/admin/{id}/edit", name="admin_tag_edit", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, Tag $tag): Response
    {
        $form = $this->createForm(TagType::class, $tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Le tag a bien été modifié.');
            return $this->redirectToRoute('admin_tag_edit', ['id' => $tag->getId()]);
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le tag n\'est pas valide. Veuillez vérifier les champs.');
        }       

        return $this->render('tag/admin/edit.html.twig', [
            'tag' => $tag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_tag_delete", methods="DELETE")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Tag $tag): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tag->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tag);
            $em->flush();
            $this->addFlash('success','Le tag a bien été supprimé.');
        }

        return $this->redirectToRoute('admin_tag_index');
    }
}
