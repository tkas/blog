<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Repository\TranslationRepository;
use App\Entity\Post;
use App\Utils\Blog;

/**
 * @Route("/{_locale}/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/admin/", name="admin_category_index", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * 
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/admin/index.html.twig', ['categories' => $categoryRepository->findAll()]);
    }
    
    /**
     * @Route("/list", name="category_list", methods="GET")
     */
    public function listMenu(CategoryRepository $categoryRepository): Response
    {
        return $this->render('category/list.menu.html.twig', ['categories' => $categoryRepository->findAll()]);
    }

    /**
     * @Route("/admin/new", name="admin_category_new", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request): Response
    {        
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            $this->addFlash('success','La catégorie a bien été ajoutée.');
            return $this->redirectToRoute('admin_category_index');
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','La catégorie n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        return $this->render('category/admin/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }
            
    /**
     * @Route("/ajaxNew", name="ajax_category_new", methods="GET|POST")
     */
    public function ajaxNew(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();           
            return $this->json($category);        
        }
        return $this->render('category/admin/ajax.form.html.twig', [
                'category' => $category,
                'form' => $form->createView(),
            ]);        
    }   
   
    /**
     * @Route("/admin/show/{slug}", name="admin_category_show", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function adminShow(Category $category): Response
    {
        return $this->render('category/admin/show.html.twig', ['category' => $category]);
    }
    
    /**
     * @Route("/{slug}", name="category_posts", methods="GET")
     */
    public function showPosts(CategoryRepository $categoryRepository, $slug,TranslationRepository $tr,request $request): Response
    {
        $cat =$categoryRepository->findOneBy(['slug' => $slug]);
        if(count($cat->getPosts()) > 0 ){
            foreach($cat->getPosts() as $k=>$post){
                
                $listPosts[] =$post;
                    
               
            }
            $translations = $tr->findTranslation(Post::class,$listPosts,$request->getLocale());
            
            $posts = Blog::translate($listPosts, $translations, Blog::postTranslationFields());
        }
               
       
        return $this->render('category/show.posts.html.twig', ['category' => $cat] );
    }       

    /**
     * @Route("/admin/{slug}/edit", name="admin_category_edit", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','La catégorie a bien été modifiée.');
            return $this->redirectToRoute('admin_category_index');
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','La catégorie n\'est pas valide. Veuillez vérifier les champs.');
        }

        return $this->render('category/admin/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_delete", methods="DELETE")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Category $category): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
            $this->addFlash('success','La catégorie a bien été supprimée.');
        }
        return $this->redirectToRoute('admin_category_index');
    }    
    
}
