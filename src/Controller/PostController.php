<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Finder\Iterator\FilenameFilterIterator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Repository\TranslationRepository;
use App\Utils\Blog;
use App\Entity\Translation;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * @Route("/{_locale}/post")
 */
class PostController extends Controller
{    
    /**
     * @Route("/admin", name="admin_post_index", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function indexAdmin(request $request,PostRepository $postRepository,TranslationRepository $tr): Response
    {       
        
       $translations = $tr->findTranslation(Post::class, null,null, 1); 
       $posts = $postRepository->findAll();
    
        $posts = Blog::translate($posts, $translations, blog::postTranslationFields());

   
        
        return $this->render('post/admin/index.html.twig', ['posts' => $posts ]);
    }

    /**
     * @Route("/admin/new", name="admin_post_new", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request ): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);      

        if ($form->isSubmitted() && $form->isValid()) {        
            $post->setUser($this->getUser());
            $file = $post->getHomeImage();
            $fileName = 'image-'.$post->getSlug().'-'.uniqid().'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('home_image_directory'),
                $fileName
                );
            
            $post->setHomeImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();            
            //dump($post);die;
            
            foreach(Blog::locale() as $k){
                    $emt = $this->getDoctrine()->getManager();                    
                    foreach(blog::postTranslationFields() as $field){
                        $getter = 'get'.ucfirst($field);                        
                    $trans = new Translation();
                    $trans->setIdRelation($post->getId());
                    $trans->setClass(Post::class);
                    $trans->setType($field);
                    $trans->setValue($post->$getter());
                    $trans->setLocale($k);
                if($k == $post->getLocale()){
                    $trans->setOriginal(1);
                }else{
                    $trans->setOriginal(0);                    
                }
                    $emt->persist($trans);               
                    }                   
                    $emt->flush();
            }
   
            
            
            $this->addFlash('success','L\'article a bien été ajouté.');
            return $this->redirectToRoute('admin_post_index');
        }  
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','L\'article n\'est pas valide. Veuillez vérifier les champs.');
        }

        return $this->render('post/admin/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{slug}", name="post_show", methods="GET")
     */
    public function show(Post $post, $slug, TranslationRepository $tr, Request $request): Response
    
    { 
      $translations = $tr->findTranslation(Post::class,$post->getId(),$request->getLocale());
      $post = blog::translate($post, $translations, blog::postTranslationFields());
      $post->setSlug($slug);  
      if($post->getStatus() != 'Publié'){
          $this->addFlash('warning', 'Vous n\'avez pas accès à cet article.');
          return $this->redirectToRoute('blog');
      }
      
      
        return $this->render('post/show.html.twig', ['post' => $post]);
    }
    
    /**
     * @Route("/admin/{id}/{slug}", name="admin_post_show", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function Adminshow(Post $post): Response
    {
        return $this->render('post/admin/show.html.twig', ['post' => $post]);
    }
    
    /**
     * @Route("/admin/{id}/{slug}/edit", name="admin_post_edit", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, Post $post,TranslationRepository $tr,$slug): Response
    {         

        
        $post->setHomeImage(new File($this->getParameter('home_image_directory').'/'.$post->getHomeImage()));
        $imageOrigine = $post->getHomeImage()->getFilename();
        $form = $this->createForm(PostType::class, $post);
            
        $form->handleRequest($request);   
        
        if($form->isSubmitted() && $form->isValid()) {
            
            if(is_null($post->getHomeImage())){            
                $fileName = $imageOrigine;                            
            }else{
                $file = $post->getHomeImage();
                $fileName = 'image-'.$post->getSlug().'-'.uniqid().'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('home_image_directory'),
                    $fileName
                    );        
            }
            $post->setHomeImage($fileName);            
            $this->getDoctrine()->getManager()->flush();
 
            
            $this->addFlash('success','L\'article a bien été modifié.');
            return $this->redirectToRoute('admin_post_index');
        }
        
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','L\'article n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        
        $translations = $tr->findTranslation(Post::class,$post->getId(),null,0);
        $transOrderer=[];
        foreach($translations as $key=>$translation){
            $transOrderer[$translation['locale']][]=$translation;
        }
      
       
        return $this->render('post/admin/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'original' => $tr->findTranslation(Post::class,$post->getId(),null,1),    
            'translations' => $transOrderer
           
       
        ]);
    }

    /**
     * @Route("/{id}", name="post_delete", methods="DELETE")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
            $this->addFlash('success','L\'article a bien été supprimé.');
        }
        return $this->redirectToRoute('admin_post_index');
    }
    
    public function listLastPostWidget(PostRepository $pr){
        return $this->render('post/list.last.post.widget.html.twig',array(
            'lastPosts' => $pr->findHomePost(3)
        ));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @Route("/admin/test/", name="admin_post_test", methods="GET|POST")
     * 
     */
    public function test(Request $request ): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $post->setUser($this->getUser());
            $file = $post->getHomeImage();
            $fileName = 'image-'.$post->getSlug().'-'.uniqid().'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('home_image_directory'),
                $fileName
                );
            
            $post->setHomeImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            //dump($post);die;
            
            foreach(Blog::locale() as $k){
                $emt = $this->getDoctrine()->getManager();
                foreach(blog::postTranslationFields() as $field){
                    $getter = 'get'.ucfirst($field);
                    $trans = new Translation();
                    $trans->setIdRelation($post->getId());
                    $trans->setClass(Post::class);
                    $trans->setType($field);
                    $trans->setValue($post->$getter());
                    $trans->setLocale($k);
                    if($k == $post->getLocale()){
                        $trans->setOriginal(1);
                    }else{
                        $trans->setOriginal(0);
                    }
                    $emt->persist($trans);
                }
                $emt->flush();
            }
            
            
            
            $this->addFlash('success','L\'article a bien été ajouté.');
            return $this->redirectToRoute('admin_post_index');
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','L\'article n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        return $this->render('post/admin/test.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
