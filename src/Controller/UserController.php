<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\User1Type;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/{_locale}/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/admin", name="admin_user_index", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/admin/index.html.twig', ['users' => $userRepository->findAll()]);
    }

    /**
     * @Route("/admin/new", name="admin_user_new", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        
        $form = $this->createForm(User1Type::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setisActive(0);
            $user->setRegistredAt(new \DateTime());
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);    
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success','L\'utilisateur bien été ajouté.');
            return $this->redirectToRoute('admin_user_index');
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le formulaire n\'est pas valide. Veuillez vérifier les champs.');
        }        

        return $this->render('user/admin/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_user_show", methods="GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function show(User $user): Response
    {
        return $this->render('user/admin/show.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/admin/{id}/edit", name="admin_user_edit", methods="GET|POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            if(!empty( $user->getPlainPassword())){                
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);       
             }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','L\'utilisateur a bien été modifié.');
            return $this->redirectToRoute('admin_user_edit', ['id' => $user->getId()]);
        }
        if($form->isSubmitted() && !$form->isValid()){
            $this->addFlash('danger','Le formulaire n\'est pas valide. Veuillez vérifier les champs.');
        }
        
        return $this->render('user/admin/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}", name="admin_user_delete", methods="DELETE")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->addFlash('success','L\'utilisateur a bien été supprimé.');
        }
        return $this->redirectToRoute('admin_user_index');
    }
    
    
    /**
     * @Route("/admin/notactive/", name="admin_user_notactive", methods="GET") 
     */
    public function notActive(UserRepository $userRepository): Response
    {
        $listusers = $userRepository->findAllNotActive(); 
        return $this->render('user/admin/notactive.html.twig', ['users' => $listusers,'nbUserNotActive' => count($listusers)]);
    }
    
    /**
     * @Route("/admin/active/{id}", name="admin_user_active", methods="GET")
     */
    public function active(user $user): Response
    {
        if($user){
        $user->setisActive(1);
        $this->getDoctrine()->getManager()->flush();
        //$this->addFlash('success','L\'utilsateur a été activé.');
        }
       
        return $this->redirectToRoute('admin_user_notactive');
    }
    
    
    
}
