<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartnerRepository")
 */
class Partner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\NotBlank()
     * @Assert\Length(min=5,max=80,minMessage="Nom du partenaire trop court",maxMessage="Nom du partenaire trop long")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=5,max=255,minMessage="Adresse du partenaire trop courte",maxMessage="Adresse du partenaire trop longue")
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=8)
     * @Assert\NotBlank()
     * @Assert\Length(min=2,max=8,minMessage="Code postal du partenaire trop court",maxMessage="Code postal du partenaire trop long")
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=24)
     * @Assert\NotBlank()
     * @Assert\Length(min=2,max=24,minMessage="Ville du partenaire trop courte",maxMessage="Ville du partenaire trop longue")
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank()
     * @Assert\Length(min=10,max=16,minMessage="Numéro de téléphone du partenaire trop court",maxMessage="Numéro de téléphone du partenaire trop long")
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=10,max=16,minMessage="Lien du partenaire trop court",maxMessage="Lien du partenaire trop long")
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\NotBlank()
     * @Assert\Length(min=2,max=16,minMessage="Siren du partenaire trop court",maxMessage="Siren du partenaire trop long")
     */
    private $siren;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=2,max=16,minMessage="Lien vers l\'image du partenaire trop court",maxMessage="Lien vers l\'image du partenaire trop long")
     */
    private $imglink;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getSiren(): ?string
    {
        return $this->siren;
    }

    public function setSiren(string $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getImglink(): ?string
    {
        return $this->imglink;
    }

    public function setImglink(string $imglink): self
    {
        $this->imglink = $imglink;

        return $this;
    }
}
