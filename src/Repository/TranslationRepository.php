<?php

namespace App\Repository;

use App\Entity\Translation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Translation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Translation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Translation[]    findAll()
 * @method Translation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TranslationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Translation::class);
    }

//    /**
//     * @return Translation[] Returns an array of Translation objects
//     */
    
    public function findTranslation($class,$idRelation=null,$locale=null,$original=null,$search=null)
    {
        $qb = $this->createQueryBuilder('t')
            ->andWhere('t.class = :class')
            ->setParameter('class', $class);
            if($idRelation){
                if(is_array($idRelation)){
                    $qb->andWhere('t.idRelation in(:idRelation)')
                    ->setParameter('idRelation', array_values($idRelation));  
                }else{
                    $qb->andWhere('t.idRelation = :idRelation')
                    ->setParameter('idRelation', $idRelation);    
                }
                   
            }
            
            
            if($locale){
             $qb->andWhere('t.locale = :locale')
                ->setParameter('locale', $locale);
            }   
            
            if($search){
                $qb->andWhere('t.value LIKE :search')
                ->setParameter('search', '%'.$search.'%');
            }   
            if($original === 0){
                $qb->andWhere('t.original = :original')
                ->setParameter('original', 0);                
            }elseif($original === 1){
                $qb->andWhere('t.original = :original')
                ->setParameter('original', 1);      
            }
           
            $qb->orderBy('t.id', 'ASC');
            return $qb->getQuery()->getArrayResult();
        ;
    }
    
    
    
    
    

    
    
    
    
   

    /*
    public function findOneBySomeField($value): ?Translation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
