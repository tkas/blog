<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }
    
    
    
    public function countNotApprovedComment()
    {
        return $this->createQueryBuilder('c')
        ->Select('count(c.id)')
        ->andWhere('c.approved = :val')
        ->setParameter('val', 'no')
        ->getQuery()
        ->getSingleScalarResult()
        ;
    }   
    
    public function countComTotal()
    {
        return $this->createQueryBuilder('c')
        ->Select('count(c.id)')       
        ->getQuery()
        ->getSingleScalarResult()
        ;
    }
    
    public function countComParent()
    {
       return $this->createQueryBuilder('c')
            ->Select('count(c.id)')
            ->andWhere('c.origin IS NOT NULL')
           ->getQuery()
           ->getSingleScalarResult();
    }
    
    public function countComResponse()
    {
        return $this->createQueryBuilder('c')
        ->Select('count(c.id)')
        ->andWhere('c.origin IS NULL')
        ->getQuery()
        ->getSingleScalarResult();
    }
    public function findForSearch($search){
        return $this->createQueryBuilder('c')
        ->where('c.content LIKE :search')        
        ->setParameter('search', '%'.$search.'%')
        ->getQuery()
        ->getResult();
    }
    
    

//    /**
//     * @return Comment[] Returns an array of Comment objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
