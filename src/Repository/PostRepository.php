<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
   
        parent::__construct($registry, Post::class);
    }
     /**
     * @return Post[] Returns an array of Post objects
     */
   
    public function findHomePost($nbPost=null,$offset = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.status = :val')
            ->setParameter('val', 'publish')           
            ->orderBy('p.createdAt', 'DESC');
            
            if($nbPost){$qb->setMaxResults($nbPost);}            
            if($offset){$qb->setFirstResult($offset) ;}         
          
            $query = $qb->getQuery();
           return $query->getResult()
        ;
    }
    
    
    /**
     * @return Post[] Returns an array of Post objects
     */
    public function findAllPostPublish()
    {
      return $this->createQueryBuilder('p')
        ->andWhere('p.status = :val')
        ->setParameter('val', 'publish')
        ->orderBy('p.createdAt', 'DESC');
        getQuery()->getResult();
    }
   
    
   /**
     * @return int Returns int of Post count
     */
    public function countAllPostPublish()
    {
        $qb = $this->createQueryBuilder('p')
                ->select('count(p.id)')
                ->Where('p.status = :val')
                ->setParameter('val', 'publish');
                $query = $qb->getQuery();
                return $query->getSingleScalarResult();
    }
    
    /**
     * @return Post 
     */
    public function findPostOnly($slug){
 
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT p,c FROM App\Entity\Post p 
             JOIN App\Entity\Comment c
             WHERE p.slug LIKE :val
             AND c.approved LIKE :approved '
             )->setParameter('val', $slug)
             ->setParameter('approved', 'no');
             return $query->execute();

        
        
  //SELECT * FROM post LEFT JOIN comment on post.id = comment.post_id WHERE slug LIKE 'super-smash-bros-ultimate' AND comment.approved LIKE 'yes' 
        
 }
 public function findForSearch($search){
        return $this->createQueryBuilder('p')
                ->where('p.id IN(:search)')               
                ->setParameter('search', array_values($search))
                ->getQuery()
                ->getResult();
 }
        


    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
 
 


 
 
 
 
}
