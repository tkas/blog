<?php

namespace App\Utils;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class CalculatriceTest extends TestCase
{
    public function testAddition(){
        $calculatrice = new Calculatrice();
        $result  = $calculatrice->addition(28, 22);
        $this->assertEquals(50,$result);
    }
    
    
    
    public function testDivision(){
        $calculatrice = new Calculatrice();
        $result  = $calculatrice->division(3, 2);
        $this->assertEquals(1.5 ,$result);
        
    }
    
    public function testSoustraction(){
        $calculatrice = new Calculatrice();
        $result = $calculatrice->soustraction(44, 20);
        $this->assertEquals(24,$result);
    }
    
    public function testMultiplication(){
        $calculatrice = new Calculatrice();
        $result = $calculatrice->multiplication(5, 2);
        $this->assertEquals(10,$result);
    }
    
    
}

