<?php 

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TranslationTest extends WebTestCase
{
    public function testTranslation(){
        $home = getcwd();
        $ds  = DIRECTORY_SEPARATOR;
        $file = $home.$ds.'translations'.$ds.'messages.en.yaml';
        
        $key = 'label.posted';
        $result = 'Posted';
        
        
        if(file_exists($file)){   
            $lines = file($file);  
            foreach ($lines as $line_num => $line) { 
                
                //$data[$line_num][strstr($line,':',true)]= str_replace("'","", strstr($line,"'"));
                
                if(strstr($line,':',true) == $key){
                    $this->assertEquals($result, str_replace("'","", strstr($line,"'")));
                }
                
            }
            
            
       
        }else{
            dump('Fichier inexistant.');
        }
        
     
        
    }
    
}
?>