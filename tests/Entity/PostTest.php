<?php 

namespace App\Entity;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class PostTest extends TestCase
{
    public function testSlug(){
        $post = new Post();
        $post->setTitle('Un titre simple');
        $this->assertEquals('un-titre-simple', $post->getSlug());
    }
    
    public function testObject(){
        $post = new Post();
        $post->setTitle('Un titre simple');
        $post->setContent('Le contenu d\'un article crée pour le test');
        $post->setStatus('publish');
        $post->setCommentStatus('activated');
        $post->setlocale('de');
        
        
        $this->assertEquals('Un titre simple',$post->getTitle());
        $this->assertEquals('Le contenu d\'un article crée pour le test',$post->getContent());
        $this->assertEquals('Publié', $post->getStatus());
        $this->assertEquals('activated', $post->getCommentStatus());       
        $this->assertEquals('de', $post->getLocale());
        $this->assertEquals('un-titre-simple',$post->getSlug());

        
    }
    
}
?>